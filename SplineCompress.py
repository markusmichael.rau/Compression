import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as spl
import pandas as pd

""" Tuning paramters of the Spline Compression
    zmin/zmax: borders of the equally spaced grid
    nnum: number of equal steps on the grid
    thresh: Remove all parts of the PDF with density below
            this threshold

    Author: Markus Michael Rau
    Email: markusmichael.rau@googlemail.com
"""


class SplCompr(object):
    """ Class to implement the Spline Compression

    """

    def __init__(self, zmin=0.0, zmax=1.5, nnum=35, thresh=0.001):
        self.zmin = zmin
        self.zmax = zmax
        self.nnum = nnum
        self.zlim = np.linspace(start=zmin, stop=zmax, num=nnum)
        self.thresh = thresh

    def compress_pdf(self, data):
        """ Compress the PDF

        """
        simple_spline = np.array([], dtype=np.float16)
        index_array = np.array([], dtype=np.uint8)
        for i in xrange(1, data.shape[1]):
            spline = spl(data[:, 0], data[:, i], ext=1)
            int_zw = spline(self.zlim)
            index_over = np.where(int_zw > self.thresh)[0]

            if len(index_over) < 2:
                print len(index_over)
                #raise ValueError('The grid spacing is too big. This can happen for instance if the PDF resembles a delta function.')
            index_array = np.append(index_array,
                                    np.array([index_over[0], index_over[-1]], dtype=np.float16))
            #which points are above the threshold?
            data_over = np.array(int_zw[index_over[0]:index_over[-1]], dtype=np.float16)
            simple_spline = np.append(simple_spline, data_over)
        return simple_spline, index_array

    def uncompress(self, spline_data, index_data):
        """ Uncompress the sparse representation
        """
        lower_counter = 0.0
        higher_counter = 0.0
        output = np.zeros((self.nnum, len(index_data)/2))
        for i in np.arange(0, len(index_data), step=2):
            curr_pdf = np.zeros((self.nnum,))
            if i == 0:
                higher_counter += (index_data[i+1] - index_data[i])
            else:
                lower_counter = higher_counter
                higher_counter += (index_data[i+1] - index_data[i])
            curr_pdf[index_data[i]:index_data[i+1]] = spline_data[lower_counter:higher_counter]
            output[:, i/2] = curr_pdf
        return output

    def get_zrange_weights(self, uncompress, zrange, z=None):
        """Calculate the weights by integrating in
        the predefined zrange

        """
        if z is None:
            z = self.zlim
        zrange_weights = np.ones((uncompress.shape[1],))
        for i in xrange(len(zrange_weights)):
            model = spl(z, uncompress[:, i], ext=1)
            zrange_weights[i] = model.integral(zrange[0], zrange[1])

        return zrange_weights

    def stack_pdf(self, zvalues, compressed_pdfs, zrange=None, weights=None):
        """Stack the PDFs with weights
        zlims: integrate PDF in the ranges
        weights: additional weights
        """

        uncompr = self.uncompress(compressed_pdfs[0],
                                  compressed_pdfs[1])
        if zrange is not None:
            zrange_weights = self.get_zrange_weights(uncompr, zrange)
            if weights is None:
                weights = zrange_weights
            else:
                weights *= zrange_weights

        data_stack = np.average(uncompr, axis=1, weights=weights)

        model = spl(self.zlim, data_stack, ext=1)
        model = spl(self.zlim, data_stack/model.integral(self.zmin, self.zmax), ext=1)
        outputpdf = model(zvalues)
        return np.column_stack((zvalues, outputpdf))

    def store_pdf(self, data, out_filename):
        """Store the compressed files

        """

        compr = self.compress_pdf(data)
        compr[0].astype('float16').tofile('PDF'+out_filename)
        compr[1].astype('uint8').tofile('IDX'+out_filename)
        print "Your PDF has been stored!"

    def read_pdf(self, out_filename):
        """Read the compressed files

        """

        pdfoutput = np.fromfile('PDF'+out_filename, dtype=np.float16)
        indexoutput = np.fromfile('IDX'+out_filename, dtype=np.uint8)

        return pdfoutput, indexoutput


#def load_hdf5(fname):
#    """Read in the hdf5 file and format it such that the Spline Compression
#    routines are working properly

#    """
#
#    data_hd5 = pd.read_hdf(fname)
#    return data_hd5.T.as_matrix()


def data_to_hdf5(data, fout):
    """ Convert a data file into a hdf5 file
        obeying the format used in the photoZ group.
    """

    hdf = pd.HDFStore(fout)
    df_data = pd.DataFrame(data[:, 1:].T)
    df_zrange = pd.DataFrame(data[:, 0])
    hdf.put('p_z', df_data, format='table')
    hdf.put('zrange', df_zrange, format='table')
    hdf.close()


class DESCompr(SplCompr):
    """This class adds functionality to the SplCompr class
    that facilitates large datasets assuming Pandas dataframe
    formats used in the DES PhotoZ group.

    I assume that the pdf files are stored as a pandas dataframe
    as a table with name p_z
    """

    def __init__(self, zmin=0.0, zmax=1.5, nnum=35, thresh=0.001):
        super(DESCompr, self).__init__(zmin, zmax, nnum, thresh)

    def compress(self, input_fname, output_fname, chunksize=None):
        """Implements pandas support for large datafiles
        """

        if input_fname[-4:] != 'hdf5':
            raise ValueError("Invalid datafile ending. I only support hdf5 files with ending hdf5.")

        #df = pd.read_hdf('test_iterator_2.hdf5', 'd1', chunksize=chunksize)
        data_z = pd.read_hdf(input_fname, 'zrange').as_matrix().flatten()

        if chunksize is None:
            data_hdf5 = pd.read_hdf(input_fname, 'p_z').T.as_matrix()
            self.store_pdf(np.column_stack((data_z, data_hdf5)), output_fname)
        else:
            data_hdf5 = pd.read_hdf(input_fname, 'p_z', chunksize=chunksize)
            vec_compressed = (np.array([]), np.array([]))
            try:
                with open('PDF'+output_fname, 'w') as pdf_file, open('IDX'+output_fname, 'w') as idx_file:
                    for el in data_hdf5:
                        data_chunck = np.column_stack((data_z, el.T.as_matrix()))
                        compr_pdf = self.compress_pdf(data_chunck)
                        vec_compressed_0 = np.concatenate((vec_compressed[0], compr_pdf[0]))
                        vec_compressed_1 = np.concatenate((vec_compressed[1], compr_pdf[1]))
                        pdf_file.write(vec_compressed_0.astype('float16').tobytes())
                        idx_file.write(vec_compressed_1.astype('uint8').tobytes())
            except IOError as e:
                print 'Operation failed: %s' % e.strerror
           # vec_compressed[0].astype('float16').tofile('PDF'+output_fname)
           # vec_compressed[1].astype('uint8').tofile('IDX'+output_fname)
            print "Your pdf has been stored!"
