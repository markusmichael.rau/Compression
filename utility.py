"""Utility functions for the compression routine
that supports loading different format of files

Markus Michael Rau
markusmichael.rau@googlemail.com

"""

#from validation import bh_photo_z_validation as pval
#import numpy as np
import pandas as pd

#def load_hdf5(fname):
#    """ Read in the hdf5 file and convert it to a numpy array
#    of the format used in the compression routine
#    """

    # require_cols = ['Z_SPEC', 'COADD_OBJECTS_ID', 'MAG_DETMODEL_I']

    # okay, dataFrame = pval.valid_file(fname, require_cols)

    # if okay is False:
    #     print "the file is not in the correct format"
    #     print "erorr message: " + dataFrame

    # zcols = [c for c in dataFrame.keys() if 'pdf_' in c]
    # zbins = np.array([float(c.split('f_')[-1]) for c in zcols])

    # max_index = int(len(zbins) + 2)
    # numpy_format = np.column_stack((zbins, dataFrame.ix[:, 2:max_index].values.T))

    # return numpy_format


def load_hdf5(fname):
    """Read in the hdf5 file and format it such that the Spline Compression
    routines are working properly

    """

    data_hd5 = pd.read_hdf(fname)
    return data_hd5.T.as_matrix()
